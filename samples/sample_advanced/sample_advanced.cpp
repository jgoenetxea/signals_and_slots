/**
 * Copyright 2019, Someone
 */
#include <iostream>

#include "signalHandler.h"

namespace cs = custom_signal;

class Button {
 public:
    cs::Signal<> on_click;
};

class Message {
 public:
    void display() const {
        std::cout << "Hello World!" << std::endl;
    }
};

class Person {
 public:
    explicit Person(std::string const &name) : name_(name) {}

    cs::Signal<std::string const&> say;

    void listen(std::string const& message) {
        std::cout << name_ << " received: " << message << std::endl;
    }

 private:
    std::string name_;
};

int main() {
    Button  button;
    Message message;

    button.on_click.connect_member(&message, &Message::display);
    button.on_click.emit();

    // Member functions with arguments
    Person alice("Alice"), bob("Bob");

    alice.say.connect_member(&bob, &Person::listen);
    bob.say.connect_member(&alice, &Person::listen);

    alice.say.emit("Have a nice day!");
    bob.say.emit("Thank you!");

    return 0;
}
