/**
 * Copyright 2019, Someone
 */
#include <iostream>

#include "signalHandler.h"

namespace cs = custom_signal;

int main() {
    // create new signal
    cs::Signal<std::string, int> signal;

    // attach a slot
    signal.connect([](std::string arg1, int arg2) {
      std::cout << arg1 << " " << arg2 << std::endl;
    });

    signal.emit("The answer:", 42);

    return 0;
}
