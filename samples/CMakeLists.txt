# Include the assets directory to download the needed binaries depending
# on the cmake configuration.
SET ( EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin/ CACHE PATH "Single output directory for building all executables." FORCE )

add_subdirectory( sample_simple )
add_subdirectory( sample_advanced )
