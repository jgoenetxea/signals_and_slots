#define CATCH_CONFIG_MAIN   // This definition generates a standard main function in compilation time, so no main function is needed.
#include <catch.hpp>        // This is the testing framework. No other library or dependencies are needed.

#include "signalHandler.h"

namespace cs = custom_signal;

template <typename... Args>
class SignalMid : public custom_signal::Signal<Args...> {
 public:
    size_t getCurrentId() {
        return custom_signal::Signal<Args...>::m_current_id;
    }

    size_t getNumOfSlots() {
        return custom_signal::Signal<Args...>::m_slots.size();
    }
};

//------------------------------------------------------------------------
//--------------- test Object registration and Factory functionality --------------------------
//
SCENARIO("Signals can be added and removed from the signal list") {
    GIVEN("A signal and a global function") {
        THEN("The connection is added") {
            // create new signal
            SignalMid<std::string, int> signal;

            REQUIRE(signal.getNumOfSlots() == 0);

            // attach a slot
            signal.connect([](std::string arg1, int arg2) {
                std::cout << arg1 << " " << arg2 << std::endl;
            });

            REQUIRE(signal.getNumOfSlots() == 1);
        }

        THEN("The signal is called") {
            // create new signal
            SignalMid<> signal;

            // attach a slot
            int numCalls = 0;
            signal.connect([&numCalls]() {
                numCalls++;
            });

            signal.emit();

            REQUIRE(numCalls == 1);
        }

        THEN("The copied signal is called") {
            // create new signal
            cs::Signal<> signal;

            // attach a slot
            int numCalls = 0;
            signal.connect([&numCalls]() {
                numCalls++;
            });

            cs::Signal<> signalCopy(signal);

            signal.emit();
            signalCopy.emit();

            REQUIRE(numCalls == 2);
        }

        THEN("The moved signal is called") {
            // create new signal
            cs::Signal<> signal;

            // attach a slot
            int numCalls = 0;
            signal.connect([&numCalls]() {
                numCalls++;
            });

            cs::Signal<> signalCopy(std::move(signal));

            signal.emit();
            signalCopy.emit();

            REQUIRE(numCalls == 1);
        }

        THEN("The assigned copy signal is called") {
            // create new signal
            cs::Signal<> signal;

            // attach a slot
            int numCalls = 0;
            signal.connect([&numCalls]() {
                numCalls++;
            });

            cs::Signal<> signalCopy;
            int numCallsCopy = 0;
            signalCopy.connect([&numCallsCopy]() {
                numCallsCopy++;
            });

            signalCopy = signal;

            signal.emit();
            signalCopy.emit();

            REQUIRE(numCalls == 2);
            REQUIRE(numCallsCopy == 0);
        }

        THEN("The assigned move signal is called") {
            // create new signal
            cs::Signal<> signal;

            // attach a slot
            int numCalls = 0;
            signal.connect([&numCalls]() {
                numCalls++;
            });

            cs::Signal<> signalCopy;
            int numCallsCopy = 0;
            signalCopy.connect([&numCallsCopy]() {
                numCallsCopy++;
            });

            signalCopy = std::move(signal);

            signal.emit();
            signalCopy.emit();

            REQUIRE(numCalls == 1);
            REQUIRE(numCallsCopy == 1);
        }

        THEN("The all slots are properly disconnected") {
            // create new signal
            SignalMid<> signal;

            REQUIRE(signal.getNumOfSlots() == 0);

            // attach three slots
            size_t slt_id1 = signal.connect([]() {
                std::cout << "Slot 1" << std::endl;
            });

            size_t slt_id2 = signal.connect([]() {
                std::cout << "Slot 2" << std::endl;
            });

            size_t slt_id3 = signal.connect([]() {
                std::cout << "Slot 3" << std::endl;
            });

            REQUIRE(signal.getNumOfSlots() == 3);

            signal.disconnect_all();

            REQUIRE(signal.getNumOfSlots() == 0);
        }

        THEN("Single slot is properly disconnected") {
            // create new signal
            SignalMid<> signal;

            REQUIRE(signal.getNumOfSlots() == 0);

            int numCalls1 = 0;
            int numCalls2 = 0;
            int numCalls3 = 0;
            int numCalls4 = 0;
            int numCalls5 = 0;

            // attach three slots
            size_t slt_id1 = signal.connect([&numCalls1]() {
                ++numCalls1;
            });

            size_t slt_id2 = signal.connect([&numCalls2]() {
                ++numCalls2;
            });

            size_t slt_id3 = signal.connect([&numCalls3]() {
                ++numCalls3;
            });

            size_t slt_id4 = signal.connect([&numCalls4]() {
                ++numCalls4;
            });

            size_t slt_id5 = signal.connect([&numCalls5]() {
                ++numCalls5;
            });

            REQUIRE(signal.getNumOfSlots() == 5);

            signal.disconnect(slt_id2);
            signal.disconnect(slt_id4);

            REQUIRE(signal.getNumOfSlots() == 3);

            signal.emit();

            REQUIRE(numCalls1 == 1);
            REQUIRE(numCalls2 == 0);
            REQUIRE(numCalls3 == 1);
            REQUIRE(numCalls4 == 0);
            REQUIRE(numCalls5 == 1);
        }

        THEN("The signal parameters are correctly parsed") {
            // create new signal
            SignalMid<std::string, int> signal;

            REQUIRE(signal.getNumOfSlots() == 0);

            // Check parameters
            std::string orig_str = "Original test!";
            int orig_int = 100345;
            std::string test_str;
            int test_int = 0;

            // attach a slot
            signal.connect([&test_str, &test_int](const std::string &_a,
                                                  const int _b) {
                test_str = _a;
                test_int = _b;
            });

            signal.emit(orig_str, orig_int);

            REQUIRE(orig_str == test_str);
            REQUIRE(orig_int == test_int);
        }
    }
}


