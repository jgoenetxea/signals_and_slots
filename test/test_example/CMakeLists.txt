#---------------------------------------------------#
#          CMake Project Configuration              #
#---------------------------------------------------#
# CMake will issue a warning if you don't set this
cmake_minimum_required( VERSION 2.8 )

if(COMMAND cmake_policy)
     cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)

# If you don't name the project, it will take the default name "sample_aamqt"
# Note: You can modify the name of the project from CmakeGUI
PROJECT( test_example )

#---------------------------------------------------#
#              OpenCV Configuration                 #
#---------------------------------------------------#
# Automatically find OpenCV files
find_package( OpenCV REQUIRED )


#---------------------------------------------------#
#               Project Configuration               #
#---------------------------------------------------#
SET ( CMAKE_DEBUG_POSTFIX d )
SET (include_dirs ${CMAKE_CURRENT_BINARY_DIR} "src/")

INCLUDE_DIRECTORIES( ${include_dirs} )
link_directories( ${CMAKE_BINARY_DIR}/bin )

#---------------------------------------------------#
#               Testing Configuration               #
#---------------------------------------------------#
SET(TESTING_HEADERS ${CMAKE_SOURCE_DIR}/test/TestingFramework/)
#MESSAGE("The path to testing framework is: ${TESTING_HEADERS}")
INCLUDE_DIRECTORIES(${TESTING_HEADERS})

#---------------------------------------------------#
#    Files of the project .cpp & .h & moc files     #
#---------------------------------------------------#

SET( PROJECT_SRCS
	src/test_example.cpp
)

SET( PROJECT_HEADERS
)

#---------------------------------------------------#
# 			EXECUTABLE	            #
#---------------------------------------------------#
ADD_EXECUTABLE( ${PROJECT_NAME}  ${PROJECT_SRCS}  ${PROJECT_HEADERS} )

#---------------------------------------------------#
#               Project Configuration               #
#---------------------------------------------------#
SET_TARGET_PROPERTIES( ${PROJECT_NAME} PROPERTIES FOLDER "dms_tests")

TARGET_LINK_LIBRARIES( ${PROJECT_NAME}  ${OpenCV_LIBS} )

#---------------------------------------------------#
#                Add automatic test                 #
#---------------------------------------------------#

IF( BUILD_TESTS )
ADD_TEST( RUN_${PROJECT_NAME}  ${EXECUTABLE_OUTPUT_PATH}/${PROJECT_NAME} )
ENDIF( BUILD_TESTS )
