# Set project name
PROJECT( signalHandlerCheck )

# Set project source files
SET( PROJECT_SRCS
  signalHandler.h
  )

# Configure project paths
INCLUDE_DIRECTORIES(
  ${signal_SRC_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_CURRENT_BINARY_DIR}
  )
LINK_DIRECTORIES( ${CMAKE_BINARY_DIR}/bin )

# Configure project output
ADD_EXECUTABLE( ${PROJECT_NAME} ${PROJECT_SRCS} dummy_main.cpp)

# set c++14 as the used c++ standard for this library
DEFINE_CPP_STANDARD_AS_14(${PROJECT_NAME})

ADD_LINT_CHECK_TO_TARGET(${PROJECT_NAME} "${PROJECT_SRCS}")
