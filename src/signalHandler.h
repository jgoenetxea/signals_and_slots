/**
 * Copyright 2019, Someone
 */
#ifndef SRC_SIGNALHANDLER_H_
#define SRC_SIGNALHANDLER_H_

#include <functional>
#include <utility>
#include <thread>
#include <map>


// A signal object may call multiple slots with the
// same signature. You can connect functions to the signal
// which will be called when the emit() method on the
// signal object is invoked. Any argument passed to emit()
// will be passed to the given functions.

namespace custom_signal {

template <typename... Args>
class Slot {
 public:
    /** Constructors **/
    // Main constructor with slot parameter
    explicit Slot(std::function<void(Args...)> const& _slot) {
        m_slot = _slot;
    }

    // copy constructor
    explicit Slot(const Slot &_other) : Slot(_other.m_slot) {
    }

    // move constructor
    explicit Slot(const Slot &&_other) : Slot(_other.m_slot) {
    }

    // destructor
    virtual ~Slot() {
        disconnect();
    }

    void disconnect() {
        // kill the thread
    }

    void call(Args... _p) const {
        // m_thread = std::thread(run, _p);
        run(std::forward<Args>(_p)...);
    }

 protected:
    std::thread m_thread;
    mutable std::function<void(Args...)> m_slot;

    void run(Args... _p) const {
        // Run the slot in an independend thread
        m_slot(std::forward<Args>(_p)...);
    }
};


template <typename... Args>
class Signal {
 public:
    /** Constructors **/
    // default constructor
    Signal() {}

    // copy constructor
    explicit Signal(const Signal &_other) : Signal() {
        m_slots = _other.m_slots;
        m_current_id = _other.m_current_id;
    }

    // move constructor
    explicit Signal(const Signal &&_other) : Signal() {
        std::swap(m_slots, _other.m_slots);
        std::swap(m_current_id, _other.m_current_id);
    }

    // destructor
    virtual ~Signal() {
        disconnect_all();
    }

    /** Operators **/
    // assignment operator
    Signal& operator=(const Signal &_other) {
        disconnect_all();
        m_slots = _other.m_slots;
        m_current_id = _other.m_current_id;
    }

    // movement operator
    Signal& operator=(const Signal &&_other) {
        std::swap(m_slots, _other.m_slots);
        std::swap(m_current_id, _other.m_current_id);
    }

    /** Connections **/
    // connects a member function to this Signal
    template <typename T>
    size_t connect_member(T *inst, void (T::*func)(Args...)) {
        return connect([=](Args... args) {
            (inst->*func)(args...);
        });
    }

    // connects a const member function to this Signal
    template <typename T>
    size_t connect_member(T *inst, void (T::*func)(Args...) const) {
        return connect([=](Args... args) {
            (inst->*func)(args...);
        });
    }

    // connects a std::function to the signal. The returned
    // value can be used to disconnect the function again
    size_t connect(std::function<void(Args...)> const& _slot) const {
        m_slots.insert(std::make_pair(++m_current_id, Slot<Args...>(_slot)));
        return m_current_id;
    }

    // disconnects a previously connected function
    void disconnect(const size_t _id) const {
        m_slots.erase(_id);
    }

    // disconnects all previously connected functions
    void disconnect_all() const {
        m_slots.clear();
        m_current_id = 0;
    }

    /** Signals **/
    // calls all connected functions
    void emit(Args... _p) {
        for (auto const& it : m_slots) {
            // it.second(std::forward<Args>(_p)...);
            it.second.call(std::forward<Args>(_p)...);
        }
    }

 protected:
    mutable std::map<size_t, Slot<Args...>> m_slots;
    mutable size_t m_current_id = 0;
};

}  // namespace custom_signal

#endif  // SRC_SIGNALHANDLER_H_
