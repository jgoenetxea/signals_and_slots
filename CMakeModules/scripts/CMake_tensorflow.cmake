#CMAKE LIBRARY USAGE MACROS:
#---------------------------------------------------#
#           Tensorflow_CC Configuration             #
#---------------------------------------------------#
MACRO( INCLUDE_PACKAGE_TENSORFLOW )
IF(UNIX)
    FIND_PACKAGE(TensorflowCC COMPONENTS Shared)
    IF(TensorflowCC_Shared_FOUND)
        SET(TENSORFLOW_LIBRARIES TensorflowCC::Shared)
        MESSAGE("Tensorflow included as shared library")
        SET (TARGET_LINK_LIBS ${TARGET_LINK_LIBS} ${TENSORFLOW_LIBRARIES})
    ELSE()
        FIND_PACKAGE(TensorflowCC REQUIRED COMPONENTS Static)
        IF(TensorflowCC_Static_FOUND)
            SET(TENSORFLOW_LIBRARIES TensorflowCC::Static)
            SET (TARGET_LINK_LIBS ${TARGET_LINK_LIBS} ${TENSORFLOW_LIBRARIES})
#            INCLUDE_DIRECTORIES(${TensorflowCC_DIR}/tensorflow)
            MESSAGE("Tensorflow included as static library")
        ELSE()
            MESSAGE(ERROR "Tensorflow library not found")
        ENDIF()
    ENDIF()
ELSEIF((MSVC14 OR MSVC15) AND CMAKE_SIZEOF_VOID_P EQUAL 8)
    SET(TENSORFLOW_VERSION 1.9 CACHE STRING "The tensorflow version (default 1.9).")
    MESSAGE(STATUS OpenCV_INCLUDE_DIRS: "${OpenCV_INCLUDE_DIRS}")
    SET(TENSORFLOW_ROOT_DIR CACHE PATH "The full path to TensorFlow's root directory")
    SET(TENSORFLOW_BUILD_DIR CACHE PATH "The full path to TensorFlow's build directory")
    OPTION(TENSORFLOW_WITH_CUDA "Turn this on if TensorFlow was built with CUDA" OFF)
    SET(TENSORFLOW_INCLUDE_DIRS
        ${TENSORFLOW_ROOT_DIR}
        ${TENSORFLOW_BUILD_DIR}
        ${TENSORFLOW_BUILD_DIR}/external/zlib_archive
        ${TENSORFLOW_BUILD_DIR}/external/gif_archive/giflib-5.1.4
        ${TENSORFLOW_BUILD_DIR}/external/png_archive
        ${TENSORFLOW_BUILD_DIR}/external/jpeg_archive
        ${TENSORFLOW_BUILD_DIR}/external/lmdb
        ${TENSORFLOW_BUILD_DIR}/external/eigen_archive
        ${TENSORFLOW_ROOT_DIR}/third_party/eigen3
        ${TENSORFLOW_BUILD_DIR}/gemmlowp/src/gemmlowp
        ${TENSORFLOW_BUILD_DIR}/jsoncpp/src/jsoncpp
        ${TENSORFLOW_BUILD_DIR}/external/farmhash_archive
        ${TENSORFLOW_BUILD_DIR}/external/farmhash_archive/util
        ${TENSORFLOW_BUILD_DIR}/external/highwayhash
        ${TENSORFLOW_BUILD_DIR}/cub/src/cub
        ${TENSORFLOW_BUILD_DIR}/external/nsync/public
        ${TENSORFLOW_BUILD_DIR}/protobuf/src/protobuf/src
        ${TENSORFLOW_BUILD_DIR}/re2/install/include
        ${TENSORFLOW_BUILD_DIR}/external/sqlite
        ${TENSORFLOW_BUILD_DIR}/snappy/src/snappy
    )
    
    SET(TENSORFLOW_LIBRARIES
        optimized ${TENSORFLOW_BUILD_DIR}/Release/tf_protos_cc.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_cc.dir/Release/tf_cc.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_cc_ops.dir/Release/tf_cc_ops.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_cc_framework.dir/Release/tf_cc_framework.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_core_cpu.dir/Release/tf_core_cpu.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_core_direct_session.dir/Release/tf_core_direct_session.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_core_framework.dir/Release/tf_core_framework.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_core_kernels.dir/Release/tf_core_kernels.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_core_lib.dir/Release/tf_core_lib.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_core_ops.dir/Release/tf_core_ops.lib
        optimized ${TENSORFLOW_BUILD_DIR}/tf_cc_while_loop.dir/Release/tf_cc_while_loop.lib
        optimized ${TENSORFLOW_BUILD_DIR}/jsoncpp/src/jsoncpp/src/lib_json/Release/jsoncpp.lib
        optimized ${TENSORFLOW_BUILD_DIR}/protobuf/src/protobuf/Release/libprotobuf.lib
        optimized ${TENSORFLOW_BUILD_DIR}/re2/src/re2/Release/re2.lib
        optimized ${TENSORFLOW_BUILD_DIR}/snappy/src/snappy/Release/snappy.lib
        optimized ${TENSORFLOW_BUILD_DIR}/gif/src/gif-build/Release/giflib.lib
        optimized ${TENSORFLOW_BUILD_DIR}/jpeg/src/jpeg-build/Release/libjpeg.lib
        optimized ${TENSORFLOW_BUILD_DIR}/lmdb/src/lmdb-build/Release/lmdb.lib
        optimized ${TENSORFLOW_BUILD_DIR}/highwayhash/src/highwayhash/Release/highwayhash.lib
        optimized ${TENSORFLOW_BUILD_DIR}/nsync/src/nsync/Release/nsync.lib
        optimized ${TENSORFLOW_BUILD_DIR}/sqlite/src/sqlite-build/Release/sqlite.lib
        optimized ${TENSORFLOW_BUILD_DIR}/farmhash/src/farmhash/Release/farmhash.lib
        optimized ${TENSORFLOW_BUILD_DIR}/fft2d/src/fft2d/Release/fft2d.lib
        debug ${TENSORFLOW_BUILD_DIR}/Debug/tf_protos_cc.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_cc.dir/Debug/tf_cc.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_cc_ops.dir/Debug/tf_cc_ops.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_cc_framework.dir/Debug/tf_cc_framework.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_core_cpu.dir/Debug/tf_core_cpu.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_core_direct_session.dir/Debug/tf_core_direct_session.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_core_framework.dir/Debug/tf_core_framework.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_core_kernels.dir/Debug/tf_core_kernels.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_core_lib.dir/Debug/tf_core_lib.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_core_ops.dir/Debug/tf_core_ops.lib
        debug ${TENSORFLOW_BUILD_DIR}/tf_cc_while_loop.dir/Debug/tf_cc_while_loop.lib
        debug ${TENSORFLOW_BUILD_DIR}/jsoncpp/src/jsoncpp/src/lib_json/Debug/jsoncpp.lib
        debug ${TENSORFLOW_BUILD_DIR}/protobuf/src/protobuf/Debug/libprotobufd.lib
        debug ${TENSORFLOW_BUILD_DIR}/re2/src/re2/Debug/re2.lib
        debug ${TENSORFLOW_BUILD_DIR}/snappy/src/snappy/Debug/snappy.lib
        debug ${TENSORFLOW_BUILD_DIR}/gif/src/gif-build/Debug/giflib.lib
        debug ${TENSORFLOW_BUILD_DIR}/jpeg/src/jpeg-build/Debug/libjpeg.lib
        debug ${TENSORFLOW_BUILD_DIR}/lmdb/src/lmdb-build/Debug/lmdb.lib
        debug ${TENSORFLOW_BUILD_DIR}/highwayhash/src/highwayhash/Debug/highwayhash.lib
        debug ${TENSORFLOW_BUILD_DIR}/nsync/src/nsync/Debug/nsync.lib
        debug ${TENSORFLOW_BUILD_DIR}/sqlite/src/sqlite-build/Debug/sqlite.lib
        debug ${TENSORFLOW_BUILD_DIR}/farmhash/src/farmhash/Debug/farmhash.lib
        debug ${TENSORFLOW_BUILD_DIR}/fft2d/src/fft2d/Debug/fft2d.lib
        wsock32.lib
        ws2_32.lib
        shlwapi.lib
    )
            
    ADD_DEFINITIONS(
        -DWIN32
        -D_WINDOWS
        -DEIGEN_AVOID_STL_ARRAY
        -DNOMINMAX
        -D_WIN32_WINNT=0x0A00
        -DLANG_CXX11
        -DCOMPILER_MSVC
        -DOS_WIN
        -D_MBCS
        -DWIN64
        -DWIN32_LEAN_AND_MEAN
        -DNOGDI
        -DPLATFORM_WINDOWS
        -DTENSORFLOW_USE_EIGEN_THREADPOOL
        -DEIGEN_HAS_C99_MATH
        -DTENSORFLOW_COMPILE_LIBRARY
        -DTENSORFLOW_USE_SNAPPY
        )
    
    IF(TENSORFLOW_VERSION GREATER_EQUAL 1.9)
    SET(TENSORFLOW_LIBRARIES ${TENSORFLOW_LIBRARIES}
        optimized ${TENSORFLOW_BUILD_DIR}/zlib/src/zlib/Release/zlibstatic.lib
        optimized ${TENSORFLOW_BUILD_DIR}/png/src/png-build/Release/libpng16_static.lib
        optimized ${TENSORFLOW_BUILD_DIR}/double_conversion/src/double_conversion/double-conversion/Release/double-conversion.lib
        debug ${TENSORFLOW_BUILD_DIR}/zlib/src/zlib/Debug/zlibstaticd.lib
        debug ${TENSORFLOW_BUILD_DIR}/png/src/png-build/Debug/libpng16_staticd.lib
        debug ${TENSORFLOW_BUILD_DIR}/double_conversion/src/double_conversion/double-conversion/Debug/double-conversion.lib
    )
    SET(ADDITIONAL_LINK_FLAGS
        "/ignore:4049 /ignore:4197 /ignore:4217 /ignore:4221 /WHOLEARCHIVE:tf_cc.lib /WHOLEARCHIVE:tf_cc_framework.lib /WHOLEARCHIVE:tf_cc_ops.lib /WHOLEARCHIVE:tf_core_cpu.lib /WHOLEARCHIVE:tf_core_direct_session.lib /WHOLEARCHIVE:tf_core_framework.lib /WHOLEARCHIVE:tf_core_kernels.lib /WHOLEARCHIVE:tf_core_lib.lib /WHOLEARCHIVE:tf_core_ops.lib /WHOLEARCHIVE:tf_stream_executor.lib /WHOLEARCHIVE:libjpeg.lib"
    )
    ELSE()
    SET(TENSORFLOW_LIBRARIES ${TENSORFLOW_LIBRARIES}
        optimized ${TENSORFLOW_BUILD_DIR}/zlib/install/lib/zlibstatic.lib
        optimized ${TENSORFLOW_BUILD_DIR}/png/install/lib/libpng12_static.lib
        debug ${TENSORFLOW_BUILD_DIR}/zlib/install/lib/zlibstaticd.lib
        debug ${TENSORFLOW_BUILD_DIR}/png/install/lib/libpng12_staticd.lib        
    )
    SET(ADDITIONAL_LINK_FLAGS
        "/ignore:4049 /ignore:4197 /ignore:4217 /ignore:4221 /WHOLEARCHIVE:tf_core_direct_session /WHOLEARCHIVE:tf_cc_framework.lib /WHOLEARCHIVE:tf_cc_ops.lib /WHOLEARCHIVE:tf_cc.lib /WHOLEARCHIVE:tf_core_cpu.lib /WHOLEARCHIVE:tf_core_direct_session.lib /WHOLEARCHIVE:tf_core_framework.lib /WHOLEARCHIVE:tf_core_kernels.lib /WHOLEARCHIVE:tf_core_lib.lib /WHOLEARCHIVE:tf_core_ops.lib /WHOLEARCHIVE:tf_stream_executor.lib /WHOLEARCHIVE:libjpeg.lib"
    )
    ENDIF()

    SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${ADDITIONAL_LINK_FLAGS}")
    
    IF(TENSORFLOW_WITH_CUDA)
        SET(CUDA_PATH $ENV{CUDA_PATH})
        SET(TENSORFLOW_INCLUDE_DIRS ${TENSORFLOW_INCLUDE_DIRS}
            ${TENSORFLOW_ROOT_DIR}/third_party/gpus
            ${CUDA_PATH}
            ${CUDA_PATH}/extras/CUPTI/include
            ${CUDA_PATH}/include
        )
        
        SET(TENSORFLOW_LIBRARIES ${TENSORFLOW_LIBRARIES}
            optimized ${TENSORFLOW_BUILD_DIR}/tf_stream_executor.dir/Release/tf_stream_executor.lib
            optimized ${TENSORFLOW_BUILD_DIR}/Release/tf_core_gpu_kernels.lib
            debug ${TENSORFLOW_BUILD_DIR}/tf_stream_executor.dir/Debug/tf_stream_executor.lib
            debug ${TENSORFLOW_BUILD_DIR}/Debug/tf_core_gpu_kernels.lib
            ${CUDA_PATH}/lib/x64/cudart_static.lib
            ${CUDA_PATH}/lib/x64/cuda.lib
            ${CUDA_PATH}/lib/x64/cublas.lib
            ${CUDA_PATH}/lib/x64/cublas_device.lib
            ${CUDA_PATH}/lib/x64/cufft.lib
            ${CUDA_PATH}/lib/x64/curand.lib
            ${CUDA_PATH}/extras/CUPTI/libx64/cupti.lib
            ${CUDA_PATH}/lib/x64/cusolver.lib
            ${CUDA_PATH}/lib/x64/cudnn.lib
        )
                
        ADD_DEFINITIONS(
            -DGOOGLE_CUDA=1
            -DTENSORFLOW_EXTRA_CUDA_CAPABILITIES=3.0,3.5,5.2
        )
    ENDIF()
    INCLUDE_DIRECTORIES(${TENSORFLOW_INCLUDE_DIRS})
    SET (TARGET_LINK_LIBS ${TARGET_LINK_LIBS} ${TENSORFLOW_LIBRARIES})
ENDIF()

ENDMACRO()
